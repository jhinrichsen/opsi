package opsi

import (
	"archive/tar"
	"bytes"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
)

// PackageParameter holds configuration for package creation
type PackageParameter struct {
	Basedir         string   // base directory for OPSI packaging
	Into            string   // Target directory for .opsi package, default "."
	Opsidir         string   // OPSI metadata directory, default "OPSI"
	Controlfilename string   // relative path into Opsidir, default "control"
	Payload         string   // OPSI payload directory, default "CLIENT_DATA"
	Workbench       string   // Temporary directory, default ""
	Metadata        Metadata // Dynamic variables, default empty/ none
	Keep            bool     // Keep interim artifacts
}

// NewPackageParameter returns default parameters for packaging.
// The only required parameter is the control file
// Defaults:
//    Into: '.'
//    Opsidir: './OPSI'
//    ControlFilename: 'control'
//    Payload: './CLIENT_DATA'
//    Workbench: "" which will use an internal temporary directory
//    Metadata: empty/ none
func NewPackageParameter() PackageParameter {
	return PackageParameter{
		Basedir:         ".",
		Into:            ".",
		Opsidir:         "OPSI",
		Controlfilename: "control",
		Payload:         "CLIENT_DATA",
		Workbench:       "",
		Metadata: Metadata{
			Entries: make(map[string]string),
		},
		Keep: false,
	}
}

// Package creates an OPSI package.
// no care has been taken to minimize interim file creation.
// TODO research io.Pipe() approach
// The OPSI control file is treated special, in that it is a required parameter,
// can optionally contain dynamic metadata (version, BUILD_ID, ...). Its
// resolved values can be used in other supporting files in the OPSI directory.
func Create(p PackageParameter) error {
	// Handle defaults
	if p.Workbench == "" {
		d, err := TmpDir()
		if err != nil {
			return err
		}
		defer func() {
			log.Debugf("removing workbench %s\n", d)
			if err := os.RemoveAll(d); err != nil {
				log.Fatal(fmt.Errorf("cannot remove %s: %v",
					d, err))
			}
		}()
		p.Workbench = d
		log.Debugf("creating workbench %v\n", p.Workbench)
	}

	// resolve metadata
	ctrlp := filepath.Join(p.Basedir, p.Opsidir, p.Controlfilename)
	ctrlf, err := ioutil.ReadFile(ctrlp)
	if err != nil {
		return fmt.Errorf("error reading control file %s: %v",
			ctrlp, err)
	}
	// metadata from controlfile
	m2, err := Parse(bytes.NewBuffer(ctrlf))
	if err != nil {
		return fmt.Errorf("error parsing control file %v: %v\n",
			ctrlp, err)
	}
	p.Metadata.Merge(m2)

	resolved, err := Resolve(string(ctrlf), p.Metadata)
	if err != nil {
		return fmt.Errorf("error resolving control file %s "+
			"using metadata %v: %v\n",
			ctrlp, p.Metadata, err)
	}

	// create OPSI.tar
	opsiTarFilename := filepath.Join(p.Workbench, "OPSI.tar")
	log.Debugf("creating %s\n", opsiTarFilename)
	opsiTarFile, err := os.Create(opsiTarFilename)
	if err != nil {
		return fmt.Errorf("cannot create %s: %s", opsiTarFilename, err)
	}
	tw := tar.NewWriter(opsiTarFile)
	writeControlfile(tw, resolved.Bytes())
	mappedOpsidir := NewDirMapper(filepath.Join(p.Basedir, p.Opsidir))
	if err := writeRecursively(tw, mappedOpsidir, p.Controlfilename); err != nil {
		return fmt.Errorf("error preparing OPSI directory to %s: %s",
			opsiTarFilename, err)
	}
	if err := tw.Close(); err != nil {
		return fmt.Errorf("error closing %s: %v", opsiTarFilename, err)
	}

	// create OPSI.tar.gz
	opsiTarGzFilename := filepath.Join(p.Workbench, "OPSI.tar.gz")
	_, err = GzipFile(opsiTarFilename, opsiTarGzFilename)
	if err != nil {
		return fmt.Errorf("cannot compress %s into %s: %v",
			opsiTarFilename, opsiTarGzFilename, err)
	}

	// create CLIENT_DATA.tar
	clientTarFilename := filepath.Join(p.Workbench, "CLIENT_DATA.tar")
	log.Debugf("creating %s\n", clientTarFilename)
	clientTarFile, err := os.Create(clientTarFilename)
	if err != nil {
		return fmt.Errorf("cannot create %s: %s",
			clientTarFilename, err)
	}
	ctw := tar.NewWriter(clientTarFile)
	mappedPayloaddir := NewDirMapper(filepath.Join(p.Basedir, p.Payload))
	if err := writeRecursively(ctw, mappedPayloaddir, ""); err != nil {
		return fmt.Errorf("cannot write %s: %v",
			clientTarFilename, err)
	}
	if err := ctw.Close(); err != nil {
		return fmt.Errorf("error closing %s: %s",
			clientTarFilename, err)
	}

	// create CLIENT_DATA.tar.gz
	clientTarGzFilename := filepath.Join(p.Workbench, "CLIENT_DATA.tar.gz")
	log.Debugf("creating %s from %s\n", clientTarGzFilename,
		clientTarFilename)
	_, err = GzipFile(clientTarFilename, clientTarGzFilename)
	if err != nil {
		return fmt.Errorf("cannot compress %s into %s: %s",
			clientTarFilename, clientTarGzFilename, err)
	}

	// create final OPSI package
	opsiPath := filepath.Join(p.Into, p.Metadata.Filename())
	opsiFile, err := os.Create(opsiPath)
	if err != nil {
		return fmt.Errorf("error creating %s: %s", opsiPath, err)
	}
	otw := tar.NewWriter(opsiFile)
	addFile(otw, "OPSI.tar.gz", opsiTarGzFilename)
	addFile(otw, "CLIENT_DATA.tar.gz", clientTarGzFilename)
	if err := otw.Close(); err != nil {
		return fmt.Errorf("error closing %s: %s", opsiPath, err)
	}
	log.Infof("created %s\n", opsiPath)
	return nil
}

func writeControlfile(tw *tar.Writer, controlfile []byte) error {
	log.Debugf("writing resolved controlfile\n")
	// synthetic, i.e. no underlying file information
	hdr := &tar.Header{
		Name:    "control",
		Mode:    0600,
		ModTime: time.Now(),
		Size:    int64(len(controlfile)),
	}
	if err := tw.WriteHeader(hdr); err != nil {
		return fmt.Errorf("cannot append header for control file: %s",
			err)
	}
	_, err := tw.Write(controlfile)
	if err != nil {
		return fmt.Errorf("cannot write content of control file: %s",
			err)
	}
	return nil
}

func writeRecursively(tw *tar.Writer, dm DirMapper, ignore string) error {
	return filepath.Walk(dm.Basedir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Name() == ignore {
				log.Debugf("ignoring %q\n", ignore)
				return nil
			}

			mappedName := dm.Right(path)
			if mappedName == "" {
				log.Debugf("skipping base directory %q\n", path)
				return nil
			}

			if info.Mode().IsDir() {
				// POSIX like a trailing / for directories
				mappedName += string(os.PathSeparator)
				hdr := &tar.Header{
					Mode:     0770,
					ModTime:  info.ModTime(),
					Name:     mappedName,
					Typeflag: tar.TypeDir,
				}
				log.Debugf("adding directory %q as %q\n",
					path, mappedName)
				if err := tw.WriteHeader(hdr); err != nil {
					return err
				}

				return nil
			}
			return addFile(tw, mappedName, path)
		})
}

func addFile(tw *tar.Writer, entry string, filename string) error {
	log.Debugf("adding %s as %s\n", filename, entry)
	info, err := os.Stat(filename)
	if err != nil {
		return fmt.Errorf("addFile: cannot stat() %s: %s", filename, err)
	}
	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("addFile: error opening %s: %s", filename, err)
	}
	defer f.Close()
	if err := addReader(tw, entry, info, f); err != nil {
		return fmt.Errorf("addFile(): error adding %s: %s", filename, err)
	}
	return nil
}

func addReader(tw *tar.Writer, entry string, info os.FileInfo,
	content io.Reader) error {

	hdr := &tar.Header{
		Name:    entry,
		Mode:    int64(info.Mode()), // uint32 -> int64 always works
		ModTime: info.ModTime(),
		Size:    info.Size(),
	}
	log.Debugf("created header %+q\n", hdr.Name)
	if err := tw.WriteHeader(hdr); err != nil {
		return err
	}
	_, err := io.Copy(tw, content)
	if err != nil {
		return err
	}
	return nil
}

// Resolve replaces placeholder with actual metadata
func Resolve(content string, m Metadata) (bytes.Buffer, error) {
	log.Debugf("---- control file before resolving ----\n")
	log.Debugf(content)
	var buf bytes.Buffer
	t := template.New("undefined")
	tmpl, err := t.Parse(content)
	if err != nil {
		return buf, fmt.Errorf("missing metadata: %v\n", err)
	}
	err = tmpl.Execute(&buf, m.Entries)
	if err != nil {
		return buf, fmt.Errorf("error executing content: %v\n", err)
	}
	log.Debugf("---- control file after resolving ----")
	log.Debugf(string(buf.Bytes()))
	return buf, nil
}
