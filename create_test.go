package opsi

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"

	log "github.com/sirupsen/logrus"
)

const (
	// Keep interim artifacts built by test cases
	keepInterim = false
)

func TestExt(t *testing.T) {
	want := ".txt "
	got := filepath.Ext("test.txt ")
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

func TestSimplePackage(t *testing.T) {
	const filename = "simple_1.2.3-4.opsi"
	params := NewPackageParameter()
	params.Opsidir = "testdata/simple/OPSI"
	params.Payload = "testdata/simple/CLIENT_DATA"
	if err := Create(params); err != nil {
		t.Fatal(err)
	}
	if !keepInterim {
		defer func(filename string) {
			log.Infof("removing %s\n", filename)
			if err := os.Remove(filename); err != nil {
				t.Fatal(err)
			}
		}(filename)
	}
	want := true
	got, err := Exists(filename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}

func TestDeepPackage(t *testing.T) {
	const filename = "deep_1.0.0-1.opsi"
	params := NewPackageParameter()
	params.Opsidir = "testdata/deep/OPSI"
	params.Payload = "testdata/deep/CLIENT_DATA"
	if err := Create(params); err != nil {
		t.Fatal(err)
	}
	if !keepInterim {
		defer func(filename string) {
			log.Infof("removing %s\n", filename)
			if err := os.Remove(filename); err != nil {
				t.Fatal(err)
			}
		}(filename)
	}
	want := true
	got, err := Exists(filename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}

	log.Debugf("reading %s", filename)
	f, err := os.Open(filename)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	tr := tar.NewReader(f)
	for {
		h, err := tr.Next()
		if err == io.EOF {
			break
		}
		log.Debugf("header: %q\n", h.Name)
		if h.Name == "OPSI.tar.gz" {
			proc(t, h, tr)
		} else if h.Name == "CLIENT_DATA.tar.gz" {
			proc(t, h, tr)
		} else {
			t.Fatalf("unexpected entry in %s: %q", filename, h)
		}
	}
}

func proc(t *testing.T, header *tar.Header, r *tar.Reader) {
	log.Debugf("processing embedded .tar.gz %s\n", header.Name)
	gr, err := gzip.NewReader(r)
	if err != nil {
		t.Fatal(err)
	}
	tr := tar.NewReader(gr)
	for {
		h, err := tr.Next()
		if err == io.EOF {
			log.Debugf("reached EOF")
			break
		}
		log.Debugf("found embedded header %s\n", h.Name)
		// Check files only
		switch h.Typeflag {
		case tar.TypeReg:
			switch h.Name {
			case "control":
				fallthrough
			case "preinst":
				fallthrough
			case "postinst":
				fallthrough
			case "helloworld.txt":
				fallthrough
			case "level2/file2.txt":
			default:
				t.Fatalf("wrong directory hierarchy: %s", h.Name)
			}
		case tar.TypeDir:
			log.Debugf("ignoring directory %s\n", h.Name)
		default:
			t.Fatalf("unexpected type %q in header %s",
				h.Typeflag, h.Name)
		}
	}
}

func TestSimplePackageOverridingVersion(t *testing.T) {
	ID := "simple"
	version := "3.2.1"
	params := NewPackageParameter()
	params.Opsidir = fmt.Sprintf("testdata/%s/OPSI", ID)
	params.Payload = fmt.Sprintf("testdata/%s/CLIENT_DATA", ID)
	params.Metadata.SetID(ID + "wm")
	params.Metadata.SetVersion(version)

	if err := Create(params); err != nil {
		t.Fatal(err)
	}
	filename := fmt.Sprintf("%swm_%s-4.opsi", ID, version)
	if !keepInterim {
		defer func(filename string) {
			log.Infof("removing %s\n", filename)
			if err := os.Remove(filename); err != nil {
				t.Fatal(err)
			}
		}(filename)
	}
	want := true
	got, err := Exists(filename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}

func TestTemplatePackageFilename(t *testing.T) {
	ID := "template"
	version := "3.2.1"
	pkg := "4"
	params := NewPackageParameter()
	params.Opsidir = fmt.Sprintf("testdata/%s/OPSI", ID)
	params.Payload = fmt.Sprintf("testdata/%s/CLIENT_DATA", ID)
	params.Metadata.SetID(ID)
	params.Metadata.SetVersion(version)
	params.Metadata.SetPackageVersion(pkg)

	if err := Create(params); err != nil {
		t.Fatal(err)
	}
	want := true
	filename := fmt.Sprintf("%s_%s-%s.opsi", ID, version, pkg)
	got, err := Exists(filename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
	if err := os.Remove(filename); err != nil {
		t.Fatal(err)

	}
}

func TestTemplatePackageResolver(t *testing.T) {
	ID := "template"
	version := "4.3.2"
	pkg := "1"
	params := NewPackageParameter()
	params.Opsidir = fmt.Sprintf("testdata/%s/OPSI", ID)
	params.Payload = fmt.Sprintf("testdata/%s/CLIENT_DATA", ID)
	params.Metadata.SetID(ID)
	params.Metadata.SetVersion(version)
	params.Metadata.SetPackageVersion(pkg)

	if err := Create(params); err != nil {
		t.Fatal(err)
	}
	want := true
	filename := fmt.Sprintf("%s_%s-%s.opsi", ID, version, pkg)
	got, err := Exists(filename)
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
	if !keepInterim {
		defer func(filename string) {
			log.Infof("removing %s\n", filename)
			if err := os.Remove(filename); err != nil {
				t.Fatal(err)

			}
		}(filename)
	}

	// read control file from generated OPSI package
	f, err := os.Open(filename)
	if err != nil {
		t.Fatal(err)
	}
	contents := make(map[string][]byte)
	tr := tar.NewReader(f)
	for {
		tarHdr, err := tr.Next()
		if err == io.EOF {
			// clean end of archive
			break
		}
		if err != nil {
			t.Fatal(err)
		}
		log.Debugf("tar entry %q\n", tarHdr.Name)

		gz, err := gzip.NewReader(tr)
		if err != nil {
			t.Fatal(err)
		}
		tgz := tar.NewReader(gz)
		for {
			hdr, err := tgz.Next()
			if err == io.EOF {
				break
			}
			if err != nil {
				t.Fatal(err)
			}
			var buf bytes.Buffer
			io.Copy(&buf, tgz)
			contents[hdr.Name] = buf.Bytes()
		}
	}

	// See if control file contains resolved variables
	buf := contents["control"]
	if buf == nil {
		log.Fatalf("error: OPSI package does not contain control file")
	}
	m, err := Parse(bytes.NewReader(buf))
	if err != nil {
		t.Fatal(err)
	}
	cmp(t, params.Metadata, m)
}

func cmp(t *testing.T, want, got Metadata) {
	if want.ID() != got.ID() {
		log.Fatalf("bad ID, want %+v but got %+v",
			want.ID(), got.ID())
	}
	if want.Version() != got.Version() {
		log.Fatalf("bad version, want %+v but got %+v",
			want.Version(), got.Version())
	}
	if want.PackageVersion() != got.PackageVersion() {
		log.Fatalf("bad package version, want %+v but got %+v",
			want.Version(), got.Version())
	}
}
