package opsi

import (
	"compress/gzip"
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

// GzipFile creates a gzip container
func GzipFile(fromFilename string, intoFilename string) (os.FileInfo, error) {
	log.Debugf("compressing %s from %s\n", intoFilename, fromFilename)
	from, err := os.Open(fromFilename)
	if err != nil {
		return nil, fmt.Errorf("error opening %s: %s", fromFilename, err)
	}
	defer from.Close()

	into, err := os.Create(intoFilename)
	if err != nil {
		return nil, fmt.Errorf("error creating %s: %s", intoFilename, err)
	}
	defer into.Close()
	w := gzip.NewWriter(into)
	n, err := io.Copy(w, from)
	if err != nil {
		return nil, fmt.Errorf("error writing to %s: %s", intoFilename, err)
	}
	if err := w.Close(); err != nil {
		return nil, fmt.Errorf("error closing %s: %s", intoFilename, err)
	}

	fi, err := os.Stat(intoFilename)
	if err != nil {
		return nil, fmt.Errorf("cannot stat() %s: %s", intoFilename, err)
	}

	var factor float64
	if fi.Size() == 0 {
		factor = 1.0
	} else {
		factor = float64(n) / float64(fi.Size())
	}
	log.Debugf("compress factor %.1f\n", factor)
	return fi, nil
}
