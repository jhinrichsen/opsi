module gitlab.com/jhinrichsen/opsi

go 1.12

require (
	github.com/cavaliercoder/go-cpio v0.0.0-20180626203310-925f9528c45e
	github.com/jhinrichsen/treeprint v1.0.0
	github.com/sirupsen/logrus v1.3.0
	github.com/urfave/cli v1.21.0
	golang.org/x/sys v0.0.0-20181029174526-d69651ed3497 // indirect
)
