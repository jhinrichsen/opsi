package opsi

import (
	"archive/tar"
	"io"
	"os"
	"strings"

	"github.com/cavaliercoder/go-cpio"
	log "github.com/sirupsen/logrus"

	"github.com/jhinrichsen/treeprint"
)

// ListContent returns filenames included in an .opsi archive.
// Supports .tar for now.
// TODO add cpio support
func ListContent(filename string) (treeprint.Tree, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer func(f *os.File) {
		if err := f.Close(); err != nil {
			log.Fatalf("error closing %v: %v\n", f, err)
		}
	}(f)
	tree := treeprint.New()
	if err := listTar(tar.NewReader(f), tree); err != nil {
		return nil, err
	}
	return tree, nil
}

// there is no common interface for structured reader such as tar, zip, and
// cpio, so we need to switch back between typed list functions
func listCpio(cr *cpio.Reader, tree treeprint.Tree) error {
	for {
		hdr, err := cr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		log.Debugf("read %s\n", hdr.Name)
		node := tree
		isCpio := strings.HasSuffix(hdr.Name, ".cpio")
		isCpioGz := strings.HasSuffix(hdr.Name, ".cpio.gz")
		if isCpio || isCpioGz {
			// use container as directory
			node = tree.AddBranch(hdr.Name)
			var embeddedR *cpio.Reader
			var er error
			if isCpio {
				embeddedR, er = CpioReaderFor(".cpio", cr)
			} else {
				embeddedR, er = CpioReaderFor(".cpio.gz", cr)
			}
			if er != nil {
				log.Fatal(er)
			}
			err := listCpio(embeddedR, node)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			node.AddNode(hdr.Name)
		}
	}
	return nil
}

// if we supported more than .tar and .tar.gz, use automatically unwrapping
// readers. For now, hardcode.
func listTar(tr *tar.Reader, tree treeprint.Tree) error {
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		node := tree
		isTar := strings.HasSuffix(hdr.Name, ".tar")
		isTarGz := strings.HasSuffix(hdr.Name, ".tar.gz")
		if isTar || isTarGz {
			// use container as directory
			node = tree.AddBranch(hdr.Name)
			var embeddedTr *tar.Reader
			var er error
			if isTar {
				embeddedTr, er = TarReaderFor(".tar", tr)
			} else {
				embeddedTr, er = TarReaderFor(".tar.gz", tr)
			}
			if er != nil {
				log.Fatal(er)
			}
			err := listTar(embeddedTr, node)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			node.AddNode(hdr.Name)
		}
	}
	return nil
}

// Writes control file to writer
func ListControlfile(tr *tar.Reader, w io.Writer) error {
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		isTar := strings.HasSuffix(hdr.Name, ".tar")
		isTarGz := strings.HasSuffix(hdr.Name, ".tar.gz")
		if isTar || isTarGz {
			// descend into OPSI container
			if !strings.HasPrefix(hdr.Name, "OPSI.tar") {
				continue
			}
			var embeddedTr *tar.Reader
			var er error
			if isTar {
				embeddedTr, er = TarReaderFor(".tar", tr)
			} else {
				embeddedTr, er = TarReaderFor(".tar.gz", tr)
			}
			if er != nil {
				log.Fatal(er)
			}
			err := ListControlfile(embeddedTr, w)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			if isControlfilename(hdr.Name) {
				if _, err := io.Copy(w, tr); err != nil {
					return nil
				}
				break
			}
		}
	}
	return nil
}
