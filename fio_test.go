package opsi

import (
	"os"
	"path/filepath"
	"testing"
)

func TestDirMapper(t *testing.T) {
	dm := NewDirMapper("testdata/deep/CLIENT_DATA")
	found := false
	filepath.Walk(dm.Basedir, func(path string, info os.FileInfo, err error) error {
		if info.Name() == "file2.txt" {
			found = true
			wantRight := "level2/file2.txt"
			gotRight := dm.Right(path)
			if wantRight != gotRight {
				t.Fatalf("right: want %s but got %s", wantRight, gotRight)
			}
		}

		return nil
	})
	if !found {
		t.Fatalf("expected folder to contain file2.txt, but it doesn't")
	}
}

var cpios = []struct {
	in   string
	want bool
}{
	{"test.cpio", true},
	{"cpio", false},
	{"f1/cpio", false},
	{"f1/t.cpio", true},
	{"test.CPIO", true},
	{"test.CpIo", true},
}

func TestIsCpio(t *testing.T) {
	for _, tt := range cpios {
		t.Run(tt.in, func(t *testing.T) {
			got := IsCpio(tt.in)
			if tt.want != got {
				t.Errorf("want %t but got %t", tt.want, got)
			}
		})
	}
}

var cpiogzs = []struct {
	in   string
	want bool
}{
	{"test.cpio.gz", true},
	{"cpio.gz", false},
	{"f1/cpio.gz", false},
	{"f1/t.cpio.gz", true},
	{"test.CPIO.GZ", true},
	{"test.CpIo.gZ", true},
}

func TestIsCpioGz(t *testing.T) {
	for _, tt := range cpiogzs {
		t.Run(tt.in, func(t *testing.T) {
			got := IsCpioGz(tt.in)
			if tt.want != got {
				t.Errorf("want %t but got %t", tt.want, got)
			}
		})
	}
}

var tars = []struct {
	in   string
	want bool
}{
	{"test.tar", true},
	{"tar", false},
	{"f1/tar", false},
	{"f1/t.tar", true},
	{"test.TAR", true},
	{"test.tAr", true},
}

func TestIsTar(t *testing.T) {
	for _, tt := range tars {
		t.Run(tt.in, func(t *testing.T) {
			got := IsTar(tt.in)
			if tt.want != got {
				t.Errorf("want %t but got %t", tt.want, got)
			}
		})
	}
}

var targzs = []struct {
	in   string
	want bool
}{
	{"test.tar.gz", true},
	{"tar.gz", false},
	{"f1/tar.gz", false},
	{"f1/t.tar.gz", true},
	{"test.TAR.GZ", true},
	{"test.tAr.gZ", true},
}

func TestIsTarGz(t *testing.T) {
	for _, tt := range targzs {
		t.Run(tt.in, func(t *testing.T) {
			got := IsTarGz(tt.in)
			if tt.want != got {
				t.Errorf("want %t but got %t", tt.want, got)
			}
		})
	}
}
