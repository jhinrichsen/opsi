package opsi

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/cavaliercoder/go-cpio"
)

// Exists returns whether the given file or directory exists or not
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false, nil
	}
	if err == nil {
		return true, nil
	}
	var mu bool
	return mu, err
}

// TmpDir returns an OPSI related temporary directory.
// Caller is responsible for removing/ housekeeping
func TmpDir() (string, error) {
	return ioutil.TempDir("", "opsi-")
}

// DirMapper can extract a hierarchy of folder names without the base directory
// itself.
type DirMapper struct {
	Basedir string
}

// NewDirMapper returns a DirMapper for given folder.
func NewDirMapper(basedir string) DirMapper {
	return DirMapper{Basedir: basedir}
}

// Left returns the original structure including base directory.
func (a *DirMapper) Left(dir string) string {
	return filepath.Join(a.Basedir, dir)

}

// Right returns the translated hierarchy without base directory.
// https://github.com/golang/go/issues/18358
func (a *DirMapper) Right(dir string) string {
	dir = strings.TrimPrefix(dir, a.Basedir)
	// remove leading / which would make it absolute
	dir = strings.TrimPrefix(dir, string(os.PathSeparator))
	return dir
}

// IsTar determines if a filename ends in .tar (or .TAR, or any combination)
func IsTar(filename string) bool {
	return ".tar" == strings.ToLower(filepath.Ext(filename))
}

// IsTarGz determines if a filename ends in .tar.gz (or .TAR.GZ, or any
// combination)
func IsTarGz(filename string) bool {
	s := filepath.Ext(filename)
	if ".gz" != strings.ToLower(s) {
		return false
	}
	return IsTar(strings.TrimSuffix(filename, s))
}

// IsCpio determines if a filename ends in .cpio (or any combination of upper-
// and lowercase)
func IsCpio(filename string) bool {
	return ".cpio" == strings.ToLower(filepath.Ext(filename))
}

// IsCpioGz determines if a filename ends in .cpio.gz (or any combination of
// upper- and lowercase)
func IsCpioGz(filename string) bool {
	s := filepath.Ext(filename)
	if ".gz" != strings.ToLower(s) {
		return false
	}
	return IsCpio(strings.TrimSuffix(filename, s))
}

// CpioReaderFor returns an appropriate reader for either .cpio or .cpio.gz
// files.
func CpioReaderFor(filename string, cr *cpio.Reader) (*cpio.Reader, error) {
	if IsCpio(filename) {
		return cpio.NewReader(cr), nil
	} else if IsCpioGz(filename) {
		cz, err := gzip.NewReader(cr)
		if err != nil {
			return nil, err
		}
		cr := cpio.NewReader(cz)
		return cr, nil
	}
	msg := "want .cpio or .cpio.gz extension but got %q\n"
	return nil, fmt.Errorf(msg, filename)
}

// TarReaderFor returns an appropriate reader for either .tar or .tar.gz files
func TarReaderFor(filename string, tr *tar.Reader) (*tar.Reader, error) {
	if IsTar(filename) {
		return tar.NewReader(tr), nil
	} else if IsTarGz(filename) {
		rz, err := gzip.NewReader(tr)
		if err != nil {
			return nil, err
		}
		rt := tar.NewReader(rz)
		return rt, nil
	}
	msg := "want .tar or .tar.gz extension but got %q\n"
	return nil, fmt.Errorf(msg, filename)
}
