package opsi

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// toml is ini-like but defines equal sign instead of colon for separating keys and values
// Two .ini packages already exist:
// - github.com/go-ini/ini
// - github.com/vaughan0/go-ini
// Both hardcode the equal sign as key-value separator, so cannot be used as
// OPSI uses colon.
// This implementation expects the optional "[Changelog]" section to be the
// last section in control file.

const (
	separator     = ":"
	sectionPrefix = "["
	sectionSuffix = "]"
)

const (
	// Controlfilename is the default OPSI naming convention
	Controlfilename = "control"
)

func isControlfilename(s string) bool {
	return s == Controlfilename
}

func isComment(line string) bool {
	return strings.HasPrefix(line, "#") ||
		strings.HasPrefix(line, ";")
}

func isEmpty(line string) bool {
	return len(line) == 0
}

func keyValue(line string) (string, string) {
	parts := strings.Split(line, separator)
	// key-value structure?
	if len(parts) != 2 {
		return "", ""
	}
	key := parts[0]
	value := parts[1]
	key = strings.TrimSpace(key)
	value = strings.TrimSpace(value)
	return key, value
}

// Parse reads metadata from control files.
// All sections and all keys are lowercase
func Parse(control io.Reader) (Metadata, error) {
	m := Metadata{
		Entries: make(map[string]string),
	}
	scanner := bufio.NewScanner(control)
	var sctn string
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line)
		if isComment(line) || isEmpty(line) {
			continue
		}

		prospectSection := section(line)
		if len(prospectSection) > 0 {
			sctn = strings.ToLower(prospectSection)
			if sctn == "changelog" {
				// Changelog is the last section, no more key value pairs to expect
				break
			}
			continue
		}
		k, v := keyValue(line)
		if k != "" {
			k = fmt.Sprintf("%s_%s", sctn, strings.ToLower(k))
			m.Entries[k] = v
		}
	}
	err := scanner.Err()
	return m, err
}

// return section name or empty string if no section
func section(line string) string {
	if strings.HasPrefix(line, sectionPrefix) && strings.HasSuffix(line, sectionSuffix) {
		line = strings.TrimPrefix(line, sectionPrefix)
		line = strings.TrimSuffix(line, sectionSuffix)
		return strings.ToLower(line)
	}
	return ""
}
