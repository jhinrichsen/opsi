package opsi

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	// KEY_ID is OPSI's product_id
	KEY_ID = "product_id"

	// KEY_VERSION is OPSI's product_version
	KEY_VERSION = "product_version"

	// KEY_PACKAGE_VERSION is OPSI's package_version
	KEY_PACKAGE_VERSION = "package_version"
)

// Metadata carries key value mappings for OPSI
type Metadata struct {
	Entries map[string]string
}

// NewMetadata will initialize from a list of key value pairs.
// Existing entries remain unchanged if any entry cannot be parsed.
func NewMetadata(entries []string, separator string) (*Metadata, error) {
	m := make(map[string]string)
	for i, s := range entries {
		kv := strings.Split(s, separator)
		if len(kv) != 2 {
			return nil, fmt.Errorf("error parsing key value pair #%d: want key=value but got %s", i, s)
		}
		m[strings.TrimSpace(kv[0])] = strings.TrimSpace(kv[1])
	}
	return &Metadata{
		Entries: m,
	}, nil
}

// ID is a shortcut for key product_id
func (a *Metadata) ID() string {
	return a.Entries[KEY_ID]
}

// ID is a shortcut for key product_id
func (a *Metadata) SetID(s string) {
	a.Entries[KEY_ID] = s
}

// DottedVersion returns the version in dotted format, e.g. 3.1.2
func (a *Metadata) DottedVersion() string {
	return toDot(a.Entries[KEY_VERSION])
}

// UnderscoredVersion returns the version in dotted format, e.g. 3_1_2
func (a *Metadata) UnderscoredVersion() string {
	return toUnderscore(a.Entries[KEY_VERSION])
}

// PackageVersion returns the package version
func (a *Metadata) PackageVersion() string {
	return a.Entries[KEY_PACKAGE_VERSION]
}

func (a *Metadata) SetPackageVersion(s string) {
	a.Entries[KEY_PACKAGE_VERSION] = s
}

// Version returns the version in dotted format, e.g. 3.1.2
func (a *Metadata) Version() string {
	return toDot(a.Entries[KEY_VERSION])
}

func (a *Metadata) SetVersion(s string) {
	a.Entries[KEY_VERSION] = s
}

// Filename derives final OPSI package name from metadata.
// <product_id>_<product_version>-<package_version>.opsi
// missing metadata will result in defaults:
// product_id: "undefined"
// product_version: 0.0.0
// package_version: 0
func (a *Metadata) Filename() string {
	return fmt.Sprintf("%s_%s-%s.opsi",
		val(a.ID(), "undefined"),
		val(a.Version(), "0.0.0"),
		val(a.PackageVersion(), "0"))
}

func val(value, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

// Merge will merge non-existent key-value pairs from m to a
// existing keys will NOT be overwritten
func (a *Metadata) Merge(m Metadata) {
	log.Tracef("merging %+v into existing metadata %+v\n",
		m.Entries, a.Entries)
	for k, v := range m.Entries {
		if _, ok := a.Entries[k]; ok {
			// warn for same key, different values
			if a.Entries[k] != v {
				log.Debugf("skipping existing metadata %s=%s "+
					"because of %s=%s\n", k, v, k, a.Entries[k])
			}
		} else {
			a.Entries[k] = v
		}
	}
}

func toDot(version string) string {
	return strings.Replace(version, "_", ".", -1)
}

func toUnderscore(version string) string {
	return strings.Replace(version, ".", "_", -1)
}
