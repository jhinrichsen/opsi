package opsi

import (
	"bytes"
	"io/ioutil"
	"os"
	"testing"
)

func TestComment(t *testing.T) {
	want := true
	got := isComment("# comment number one")
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}

func TestSection(t *testing.T) {
	want := "sctn1"
	got := section("[Sctn1]")
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}

func TestProductVersion(t *testing.T) {
	want := "1.2.3"
	f, err := ioutil.ReadFile("testdata/simple/OPSI/control")
	if err != nil {
		t.Fatal(err)
	}
	r := bytes.NewReader(f)
	m, err := Parse(r)
	if err != nil {
		t.Fatal(err)
	}
	got := m.Entries["product_version"]
	if want != got {
		t.Fatalf("want %q but got %q\n", want, got)
	}
}

func TestParseControlfile(t *testing.T) {
	f, err := os.Open("testdata/simple/OPSI/control")
	if err != nil {
		t.Fatal(err)
	}
	defer func(f *os.File) {
		if err := f.Close(); err != nil {
			t.Fatal(err)
		}
	}(f)
	m, err := Parse(f)
	if err != nil {
		t.Fatal(err)
	}
	want := "1.2.3"
	got := m.Version()
	if want != got {
		t.Fatalf("want %q but got %q\n", want, got)
	}
}

func TestControlfileTemplate(t *testing.T) {
	const (
		key     = "product_version"
		version = "0.0.0"
	)

	// Parse template control file
	buf, err := ioutil.ReadFile("testdata/template/OPSI/control")
	if err != nil {
		t.Fatal(err)
	}

	m, err := NewMetadata([]string{
		"product_version=0.0.0",
	}, "=")
	if err != nil {
		t.Fatal(err)
	}
	m.SetVersion(version)
	r, err := Resolve(string(buf), *m)
	if err != nil {
		t.Fatal(err)
	}

	// Parse resolved template file
	m2, err := Parse(&r)
	if err != nil {
		t.Fatal(err)
	}
	want := version
	got := m2.Version()
	if want != got {
		t.Fatalf("want %q but got %q\n", want, got)
	}
}
