package opsi

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/cavaliercoder/go-cpio"
	"github.com/jhinrichsen/treeprint"
	log "github.com/sirupsen/logrus"
)

const sampleCpio = "testdata/cpio/sugar-hello-world-6-14.fc30.noarch.cpio"
const filezillaOpsiFilename = "testdata/filezilla/dfn_filezilla_3.46.3-1.opsi"

func TestListCpioHeader(t *testing.T) {
	f, err := os.Open(sampleCpio)
	if err != nil {
		t.Fatalf("error opening %s: %v", sampleCpio, err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			t.Fatalf("error closing %s: %v", sampleCpio, err)
		}
	}()
	want := 15
	got := 0
	r := cpio.NewReader(f)
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf("error reading header: %v", err)
		}
		got++
		log.Debugf("read header %s: %+v\n", filepath.Base(hdr.Name), hdr)
	}
	if want != got {
		t.Fatalf("want %d headers but got %d", want, got)
	}
}

func TestListCpio(t *testing.T) {
	buf, err := ioutil.ReadFile("testdata/filezilla/want_tree.txt")
	if err != nil {
		t.Fatal(err)
	}
	want := string(buf)

	f, err := os.Open(filezillaOpsiFilename)
	if err != nil {
		t.Fatalf("error reading %s: %v", filezillaOpsiFilename, err)
	}

	r := cpio.NewReader(f)
	tree := treeprint.New()
	if err := listCpio(r, tree); err != nil {
		t.Fatalf("error listing %s: %v", filezillaOpsiFilename, err)
	}
	got := tree.String()
	if want != got {
		t.Fatalf("want %s but got %s", want, got)
	}
}
