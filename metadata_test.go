package opsi

import "testing"

func TestDefaultOpsiPackageFilename(t *testing.T) {
	want := "undefined_0.0.0-0.opsi"
	m := Metadata{}
	got := m.Filename()
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}

func TestFinalOpsiPackageFilename(t *testing.T) {
	want := "p1_1.2.3-4.opsi"
	m := Metadata{
		Entries: map[string]string{
			"package_version": "4",
			"product_version": "1.2.3",
			"product_id":      "p1",
		},
	}

	got := m.Filename()
	if want != got {
		t.Fatalf("want %v but got %v\n", want, got)
	}
}
