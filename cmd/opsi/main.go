package main

import (
	"archive/tar"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/jhinrichsen/opsi"

	"github.com/urfave/cli"
)

// Version is populated from git during link step
var Version = "undefined"

// Commit is populated from git during link step
var Commit = "undefined"

var basedir string

const (
	keepOption            = "keep"
	listControlFileOption = "controlfile"
	outdirOption          = "outdir"
	workbenchOption       = "workbench"
)

func create(ctx *cli.Context) error {
	log.Debugf("creating OPSI package")
	p := opsi.NewPackageParameter()
	p.Basedir = basedir
	p.Into = ctx.String(outdirOption)
	if len(p.Into) > 0 { // assume local directory is already there
		log.Infof("creating output directory %q\n", p.Into)
		if err := os.MkdirAll(p.Into, 0770); err != nil {
			return fmt.Errorf("cannot create directory %q\n", err)
		}
	}
	p.Keep = ctx.Bool(keepOption)
	p.Workbench = ctx.String(workbenchOption)
	if len(p.Workbench) == 0 {
		// Setup workbench in temporary directory
		d, err := ioutil.TempDir("", "opsi-")
		if err != nil {
			return err
		}
		p.Workbench = d

		if !p.Keep {
			log.Debugf("registering scratch directory for removal")
			defer func(dir string) {
				log.Infof("removing temporary workbench %q\n", dir)
				if err := os.RemoveAll(dir); err != nil {
					log.Fatalf("error removing workbench %q: %v\n", dir, err)
				}
			}(p.Workbench)
		}
	}

	log.Debugf("using workbench %q\n", p.Workbench)

	// Parse metadata
	m, err := opsi.NewMetadata(ctx.Args(), "=")
	if err != nil {
		return fmt.Errorf("error parsing metadata %v", ctx.Args())
	}
	p.Metadata = *m
	if err := opsi.Create(p); err != nil {
		return fmt.Errorf("error creating OPSI package: %v", err)
	}
	return nil
}

func list(ctx *cli.Context) error {
	log.Debugf("using base directory %q\n", basedir)
	filenames := ctx.Args()
	if len(filenames) == 0 {
		matches, err := filepath.Glob(filepath.Join(basedir, "*.opsi"))
		if err != nil {
			fmt.Fprintf(os.Stderr,
				"error searching for .opsi files: %v\n", err)
		}
		filenames = matches
	} else {
		for i := range filenames {
			filenames[i] = filepath.Join(basedir, filenames[i])
		}
	}
	if len(filenames) == 0 {
		fmt.Fprintf(os.Stderr, "no arguments, no local .opsi packages"+
			", what do you want to list?\n")
		os.Exit(3)
	}
	for _, filename := range filenames {
		if ctx.Bool(listControlFileOption) {
			f, err := os.Open(filename)
			if err != nil {
				log.Fatal(err)
			}
			defer func() {
				if err := f.Close(); err != nil {
					log.Fatal(err)
				}
			}()
			tr := tar.NewReader(f)
			if err := opsi.ListControlfile(tr,
				os.Stdout); err != nil {
				log.Fatal(err)
			}
		} else {
			tree, err := opsi.ListContent(filename)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(tree)
		}
	}
	return nil
}

func globalBefore(ctx *cli.Context) error {
	if ctx.GlobalBool("debug") {
		log.SetLevel(log.DebugLevel)
		log.Debugf("entering debug level")
	}
	return nil
}

func init() {
	// log.SetFormatter(&log.JSONFormatter{})
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("opsi version=%s commit=%s\n", c.App.Version, Commit)
	}
}

func main() {
	app := cli.NewApp()
	app.CommandNotFound = func(ctx *cli.Context, cmd string) {
		fmt.Fprintf(os.Stderr, "unknown subcommand %q, -h for usage\n",
			cmd)
		os.Exit(2)
	}
	app.Commands = []cli.Command{
		{
			Action:  list,
			Aliases: []string{"l"},
			Before:  globalBefore,
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  listControlFileOption,
					Usage: "show control file",
				},
			},
			Name:  "list",
			Usage: "Lists existing OPSI package(s)",
		},
		{
			Action:  create,
			Aliases: []string{"c"},
			Before:  globalBefore,
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  keepOption,
					Usage: "keep interim artifacts",
				},
				cli.StringFlag{
					Name:  outdirOption + ", o",
					Usage: "output directory",
				},
				cli.StringFlag{
					Name:  "workbench",
					Usage: "location for interim artifacts, empty for system wide temporary directory",
				},
			},
			Name:  "create",
			Usage: "Creates OPSI package",
		},
	}
	app.Compiled = time.Now()
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "debug, d",
			Usage: "output on debug level",
		},
		cli.StringFlag{
			Destination: &basedir,
			Name:        "basedir, C",
			Usage:       "base directory for all operations (think make -C, git -C)",
		},
	}
	app.Name = "opsi"
	app.Usage = fmt.Sprintf("OPSI utilities %s (%s)", Version, Commit)
	// v2 only? app.UseShortOptionHandling = true
	app.Version = Version

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
